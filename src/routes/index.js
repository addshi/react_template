import React from "react";
import {BrowserRouter as Router, Redirect, Route} from 'react-router-dom';
import Test from "@/pages/home/test";
import About from "@/pages/about/about";

export default class Routes extends React.Component {

    render() {
        return (
            <Router>
                <Route path="/about" component={About}/>
                <Route path="/test" component={Test}/>
            </Router>
        )
    }
}
