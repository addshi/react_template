import * as React from "react";
import {requestByGet} from "@/utils/request";
import { NavBar, Icon } from 'antd-mobile';

export default class About extends React.Component {
    constructor(props) {
        super(props);

    }

    async componentDidMount() {
        const data = await requestByGet("");
        console.log(data);
    }

    render() {
        return (<div>
            <NavBar
                mode="light"
                icon={<Icon type="left" />}
                onLeftClick={() => console.log('onLeftClick')}
                rightContent={[
                    <Icon key="0" type="search" style={{ marginRight: '16px' }} />,
                    <Icon key="1" type="ellipsis" />,
                ]}
            >NavBar</NavBar>
        </div>)
    }
}
