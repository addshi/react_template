import * as React from "react";
import testCss from "@/assets/scss/test.scss"
import {Button} from "antd-mobile";

export default class Test extends React.Component {
    render() {
        return (<div className={testCss["red-text"]}>
            <Button>Start</Button>
        </div>)
    }
}
