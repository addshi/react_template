import React from 'react';
import common from '@/assets/common.scss';
import Routes from "@/routes";

function App() {
    return (
        <div className={common.App}>
            <Routes/>
            <footer>footer</footer>
        </div>
    );
}

export default App;
