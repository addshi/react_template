import axios from "axios";
import {BACK_END_URL} from "@/properties";

export async function requestByPost(url, params, config = {}) {
    const {data} = await axios.post(`${BACK_END_URL}/${url}`, params, config);
    return data;
}

export async function requestByGet(url, params, config = {}) {
    const {data} = await axios.get(`${BACK_END_URL}/${url}`, {
        params,
        ...config
    });
    return data;
}
